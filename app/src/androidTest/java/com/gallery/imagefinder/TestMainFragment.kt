package com.gallery.imagefinder

import android.app.Application
import android.os.Handler
import android.os.Looper
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.gallery.imagefinder.ui.main.view.fragment.MainFragment
import com.gallery.imagefinder.ui.main.view.viewModel.MainViewModel
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations


@RunWith(AndroidJUnit4::class)
class TestMainFragment {
    @get:Rule
    val rule = InstantTaskExecutorRule()
    private lateinit var viewModel: MainViewModel
    @Mock
    private lateinit var mockedApplication: Application

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        launchFragmentInContainer<MainFragment>(
            fragmentArgs = null, // Bundle
            themeResId = R.style.AppTheme,
            factory = null // FragmentFactory
        )
        viewModel = MainViewModel(mockedApplication)
    }

    @Test
    fun Test_ScreenFirst() {
        Looper.getMainLooper()
        Looper.prepare()
        Handler().postDelayed({
            onView(ViewMatchers.withId(R.id.llImageNotFound)).check(
                ViewAssertions.matches(ViewMatchers.isDisplayed())
            )

        },1000)
    }

    @Test
    fun Test_SearchForImages() {
        Looper.getMainLooper()
        Looper.prepare()
        Handler().postDelayed({
            onView(ViewMatchers.withId(R.id.llImageNotFound)).check(
                ViewAssertions.matches(ViewMatchers.isDisplayed())
            )

            onView(allOf(ViewMatchers.withId(R.id.app_bar_search)))
                .perform(click())

        },1000)
    }
}