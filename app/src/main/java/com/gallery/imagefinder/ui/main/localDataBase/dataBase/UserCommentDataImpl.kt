package com.gallery.imagefinder.ui.main.localDataBase.dataBase

import com.gallery.imagefinder.ui.main.localDataBase.listener.IRoomDao
import com.gallery.imagefinder.ui.main.localDataBase.listener.IUserComment
import com.gallery.imagefinder.ui.main.localDataBase.tableSchema.ImageCommentData
import javax.inject.Inject

class UserCommentDataImpl @Inject constructor(private val roomDao: IRoomDao) :
    IUserComment {

    override fun setUserCommentDetails(imageComment: ImageCommentData): Long {
        return roomDao.setUserCommentDetails(imageComment);
    }

    override fun getUserCommentDetails(): List<ImageCommentData> {
        return roomDao.getUserCommentDetails();
    }

    override fun getUserCommentDetailsById(imageId:String): List<ImageCommentData> {
        return roomDao.getUserCommentDetailsByID(imageId)
    }

}
