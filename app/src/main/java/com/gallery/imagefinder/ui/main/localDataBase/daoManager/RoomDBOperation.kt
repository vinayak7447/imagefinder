package com.gallery.imagefinder.ui.main.localDataBase.daoManager


import com.gallery.imagefinder.ui.main.localDataBase.di.component.IDBComponent
import com.gallery.imagefinder.ui.main.localDataBase.di.injector.IDBInjector
import com.gallery.imagefinder.ui.main.localDataBase.listener.IUserComment
import com.gallery.imagefinder.ui.main.localDataBase.tableSchema.ImageCommentData
import javax.inject.Inject

object RoomDBOperation : IDBInjector {

    var iUserCommentDetails: IUserComment? = null
        @Inject set(value) {
            field = value
        }

    override fun roomDBInjector(idbComponent: IDBComponent) {
       idbComponent.injectDetails(this)
    }

    @JvmStatic
    fun setCommentAtDB(imageComment: ImageCommentData){
        iUserCommentDetails?.setUserCommentDetails(imageComment)
    }

    @JvmStatic
    fun getAllCommentFromDB(): List<ImageCommentData>? {
        return iUserCommentDetails?.getUserCommentDetails()
    }

    @JvmStatic
    fun geCommentByImageIdFromDB(imageID:String): List<ImageCommentData>? {
        return iUserCommentDetails?.getUserCommentDetailsById(imageID)
    }


}