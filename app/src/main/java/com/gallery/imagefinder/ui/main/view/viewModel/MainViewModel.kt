package com.gallery.imagefinder.ui.main.view.viewModel

import android.app.Application
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.gallery.imagefinder.R
import com.gallery.imagefinder.ui.main.dataModel.imageFinderApi.ObjImageFinderResponse
import com.gallery.imagefinder.ui.main.listener.IMainFragmentView
import com.gallery.imagefinder.ui.main.listener.IOnGetParserResponseListener
import com.gallery.imagefinder.ui.main.networkService.serviceDao.NetworkServiceDao
import com.gallery.imagefinder.ui.main.utils.Constant
import com.gallery.imagefinder.ui.main.utils.Utils

class MainViewModel(application: Application) : AndroidViewModel(application), IMainFragmentView.IViewModelImpl, IOnGetParserResponseListener {
    var mContext: Application

    var imageListObserver: MutableLiveData<ObjImageFinderResponse> = MutableLiveData()
    var layoutVisibility = ObservableField<Boolean>(false)
    var gridVisibility = ObservableField<Boolean>(false)
    var imageIconChange = ObservableField<Drawable>()
    var textChange = ObservableField<String>()

    init {
        mContext = application
    }

    override fun callImageFinderApi(searchContent:String) {
        var request=HashMap<String,String>()
        request.put(Constant.QUERY_PARAMETER,searchContent)
        NetworkServiceDao.apiNetworkServiceReq(Constant.IMAGE_LIST_API_END_POINT,request,this)
    }

    override fun onGetSuccessResponse(response: String) {
        try {
            imageListObserver.value= Utils.getDeserializeResponse(response)
        } catch (e: Exception) {
            Log.e("",e.toString())
        }
    }

    override fun onGetFailureResponse(response: String) {
        try {
            Utils.updateScreenView(this,mContext, R.drawable.ic_mood_bad_black_24dp, R.string.txt_error)
            Log.e("Api error res",response)
        } catch (e: Exception) {
            Log.e("",e.toString())
        }
    }
}
