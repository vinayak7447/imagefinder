package com.gallery.imagefinder.ui.main.networkService.di.interceptor
import com.gallery.imagefinder.ui.main.utils.Constant
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

class AuthenticationInterceptor @Inject constructor() :
    Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response? {

        var request = chain.request()
            var authenticationRequest = request.newBuilder()
                .header(Constant.AUTH_HEADER, Constant.AUTH_HEADER_VALUE).build()
            return chain.proceed(authenticationRequest)

    }


}