package com.gallery.imagefinder.ui.main.application

import android.app.Application
import com.gallery.imagefinder.ui.main.localDataBase.daoManager.RoomDBOperation
import com.gallery.imagefinder.ui.main.localDataBase.di.component.DaggerIDBComponent
import com.gallery.imagefinder.ui.main.networkService.di.component.DaggerServiceComponent
import com.gallery.imagefinder.ui.main.networkService.serviceDao.NetworkServiceDao
import com.gallery.imagefinder.ui.main.utils.Constant

class ImageApplication : Application(){

    override fun onCreate() {
        super.onCreate()
        NetworkServiceDao.apiServiceInjector(DaggerServiceComponent.factory().create(Constant.BASE_URL))
        RoomDBOperation.roomDBInjector(DaggerIDBComponent.factory().create(this,Constant.DATA_BASE_NAME))
    }
}