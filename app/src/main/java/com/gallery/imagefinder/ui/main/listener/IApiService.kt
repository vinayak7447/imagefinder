package com.gallery.imagefinder.ui.main.listener



import okhttp3.RequestBody

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


/*
* Retrofit Endpoints
 */
interface IApiService {
    @GET
    fun apiNetworkServiceGet(@Url url:String,@QueryMap(encoded = true) list: Map<String, String>):Call<ResponseBody>

}
