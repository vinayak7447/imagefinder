package com.gallery.imagefinder.ui.main.view.activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.gallery.imagefinder.R;
import com.gallery.imagefinder.databinding.ImagePreviewActivityBinding;
import com.gallery.imagefinder.ui.main.listener.IPreviewImageView;
import com.gallery.imagefinder.ui.main.utils.Constant;
import com.gallery.imagefinder.ui.main.utils.Utils;
import com.gallery.imagefinder.ui.main.view.viewModel.PreviewActivityViewMode;

public class ImagePreviewActivity extends AppCompatActivity implements IPreviewImageView.IPreviewImageImpl {
    Bundle bundle;
    ImagePreviewActivityBinding imagePreviewActivityBinding;
    PreviewActivityViewMode previewActivityViewMode;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        previewActivityViewMode= ViewModelProviders.of(this).get(PreviewActivityViewMode.class);
        imagePreviewActivityBinding = DataBindingUtil.setContentView(this, R.layout.image_preview_activity);
        imagePreviewActivityBinding.setPreviewActivity(previewActivityViewMode);
        imagePreviewActivityBinding.setLifecycleOwner(this);
        previewActivityViewMode.iPreviewImage=this;
        getBundleData();
        init();
    }

    private void init() {
        if(bundle!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(bundle.getString(Constant.BUNDLE_IMAGE_TITLE_TAG));
            previewActivityViewMode.imageUrlUpdated(bundle.getString(Constant.BUNDLE_IMAGE_TAG));
            previewActivityViewMode.getCommentFromDB(bundle.getString(Constant.BUNDLE_ID_TAG));
        }
    }

    public void getBundleData(){
        bundle=getIntent().getBundleExtra(Constant.BUNDLE_TAG);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSubmitComment() {
        Utils.hideKeyBoard(imagePreviewActivityBinding.getRoot(),this);
        if (!imagePreviewActivityBinding.edtComment.getText().toString().isEmpty())
            previewActivityViewMode.addCommentAtDB(bundle.getString(Constant.BUNDLE_ID_TAG), imagePreviewActivityBinding.edtComment.getText().toString());
    }
}
