package com.gallery.imagefinder.ui.main.networkService.di.module

import com.gallery.imagefinder.ui.main.listener.IApiService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


@Module(includes = [NetworkModule::class])
class ServiceModule() {

    @Provides
    internal fun getApiInterface(retroFit: Retrofit): IApiService {
        return retroFit.create(IApiService::class.java)
    }


    @Provides
    internal fun getRetrofit(okHttpClient: OkHttpClient, serviceBaseURL: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(serviceBaseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }
}