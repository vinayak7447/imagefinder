package com.gallery.imagefinder.ui.main.view.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gallery.imagefinder.R
import com.gallery.imagefinder.databinding.MainFragmentBinding
import com.gallery.imagefinder.ui.main.view.activity.ImagePreviewActivity
import com.gallery.imagefinder.ui.main.view.adapter.ImageListAdapter
import com.gallery.imagefinder.ui.main.listener.IMainFragmentView
import com.gallery.imagefinder.ui.main.utils.Constant
import com.gallery.imagefinder.ui.main.utils.Utils
import com.gallery.imagefinder.ui.main.view.viewModel.MainViewModel


class MainFragment : Fragment(), IMainFragmentView {
    private lateinit var viewModel: MainViewModel
    lateinit var mainFragmentBinding: MainFragmentBinding
    var mHandler=Handler()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        mainFragmentBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.main_fragment, container, false)
        mainFragmentBinding.mainFragment = viewModel
        mainFragmentBinding.setLifecycleOwner(this)
        initView()
        return mainFragmentBinding.root
    }

    override fun initView() {
        setHasOptionsMenu(true)
        Utils.updateScreenView(viewModel,activity!!,R.drawable.ic_mood_black_24dp,R.string.hello_msg)
        observerImageList()
    }

    fun observerImageList(){
        viewModel.imageListObserver.observe(viewLifecycleOwner, Observer { objResponse->
            if(objResponse.data.size>0) {
                viewModel.layoutVisibility.set(false)
                mainFragmentBinding.rvImageListRecyclerView.setHasFixedSize(true)
                mainFragmentBinding.rvImageListRecyclerView.adapter =
                    ImageListAdapter(
                        objResponse,
                        this,
                        activity!!
                    )
                viewModel.gridVisibility.set(true)
            }else{
                Utils.updateScreenView(viewModel,activity!!,R.drawable.ic_mood_bad_black_24dp,R.string.image_not_found_msg)
            }
        })
    }

    override fun onListItemClickListener(itemSelectedValue: Bundle) {
        startActivity(Intent(context,
            ImagePreviewActivity::class.java).putExtra(Constant.BUNDLE_TAG,itemSelectedValue))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        activity!!.menuInflater.inflate(R.menu.main, menu)
        val search_item = menu.findItem(R.id.app_bar_search)
        val searchView = search_item.actionView as SearchView
        searchView.setFocusable(false)
        searchView.setQueryHint(resources.getString(R.string.txt_search))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
            return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                mHandler.removeCallbacksAndMessages(null)
                mHandler.postDelayed(Runnable {
                    newText?.let {
                        Utils.hideKeyBoard(mainFragmentBinding.getRoot(), activity!!)
                        if(Utils.checkInternetConnection(activity!!)) {
                            if(!newText.isEmpty()) {
                                viewModel.callImageFinderApi(it)
                            }
                        }else{
                            Utils.updateScreenView(viewModel,activity!!,R.drawable.ic_wifi_black_24dp,R.string.no_internet)
                        }
                    }
                }, 2000)
                return true
            }

        })
    }

}
