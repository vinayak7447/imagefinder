package com.gallery.imagefinder.ui.main.dataModel.imageFinderApi

data class ObjImageFinderResponse(
    val `data`: List<Data>,
    val status: Int,
    val success: Boolean
)