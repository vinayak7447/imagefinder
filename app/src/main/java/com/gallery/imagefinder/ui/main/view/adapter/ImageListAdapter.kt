package com.gallery.imagefinder.ui.main.view.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.gallery.imagefinder.R
import com.gallery.imagefinder.ui.main.dataModel.imageFinderApi.ObjImageFinderResponse
import com.gallery.imagefinder.ui.main.listener.IMainFragmentView
import com.gallery.imagefinder.ui.main.utils.Constant
import kotlinx.android.synthetic.main.image_list_item_view.view.*

class ImageListAdapter(
    val imageListRes: ObjImageFinderResponse,
    var iMainFragmentView: IMainFragmentView,
    var context: Context
): RecyclerView.Adapter<ImageListAdapter.ImageListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageListViewHolder {
        return ImageListViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.image_list_item_view, parent, false)
        )
    }

    override fun getItemCount()= imageListRes.data.size

    override fun onBindViewHolder(holder: ImageListViewHolder, position: Int) {
        var listItem=imageListRes.data.get(position)
        var imageLink=""
        var imageId=""
        var imageTitle=""
        try {
            imageLink=listItem.images.get(0).link
            imageId=listItem.images.get(0).id
            imageTitle=listItem.title
        }catch (Ex:Exception){
            imageLink=listItem.link
            imageId=listItem.id
            imageTitle=listItem.title
        }

        Glide.with(context)
            .asBitmap()
            .load(imageLink)
            .centerCrop()
            .placeholder(R.drawable.ic_image_icon)
            .error(R.drawable.ic_image_icon)
            .fallback(R.drawable.ic_image_icon)
            .dontAnimate()
            .skipMemoryCache(true)
            .into(holder.view.itemImageView)

        holder.view.setOnClickListener(View.OnClickListener {
            var itemSelectedValue=Bundle()
            itemSelectedValue.putString(Constant.BUNDLE_IMAGE_TAG,imageLink)
            itemSelectedValue.putString(Constant.BUNDLE_ID_TAG,imageId)
            itemSelectedValue.putString(Constant.BUNDLE_IMAGE_TITLE_TAG,imageTitle)
            iMainFragmentView.onListItemClickListener(itemSelectedValue)
        })
    }

    class ImageListViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}