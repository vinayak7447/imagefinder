package com.gallery.imagefinder.ui.main.networkService.serviceDao

import com.gallery.imagefinder.ui.main.networkService.di.component.ServiceComponent
import com.gallery.imagefinder.ui.main.networkService.di.injector.IApiServiceInjector
import com.gallery.imagefinder.ui.main.listener.IApiService
import com.gallery.imagefinder.ui.main.listener.INetworkServiceListener
import com.gallery.imagefinder.ui.main.listener.IOnGetParserResponseListener
import com.gallery.imagefinder.ui.main.utils.Constant
import com.gallery.imagefinder.ui.main.utils.Utils
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

object NetworkServiceDao: IApiServiceInjector, INetworkServiceListener {


    var iApiService: IApiService? = null
        @Inject set(value) {
            field = value
        }

    override fun apiServiceInjector(serviceComponent: ServiceComponent) {
        serviceComponent.injectNetworkService(this)
    }

    override fun apiNetworkServiceReq(apiEndPoint:String, request:Any, onGetResponse: IOnGetParserResponseListener) {

        var apiServiceGenerator = iApiService!!.apiNetworkServiceGet(apiEndPoint, request as Map<String, String>)

        apiServiceGenerator.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                onGetResponse.onGetFailureResponse(Utils.httpServerError(t))
            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                if (response.isSuccessful && response.code().toString().equals(Constant.SUCCESS_CODE)) {
                    response.body()!!.string()
                        ?.let { onGetResponse.onGetSuccessResponse(it) }
                }
            }
        })
    }
}