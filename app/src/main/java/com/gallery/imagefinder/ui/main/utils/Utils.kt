package com.gallery.imagefinder.ui.main.utils

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.gallery.imagefinder.ui.main.dataModel.imageFinderApi.ObjImageFinderResponse
import com.google.gson.Gson
import com.gallery.imagefinder.ui.main.view.viewModel.MainViewModel


object Utils {

    fun httpServerError(t: Throwable): String {
        return t.message.toString()
    }

    fun getDeserializeResponse(response: String): ObjImageFinderResponse? {
        return Gson().fromJson(
            response,
            ObjImageFinderResponse::class.java
        )
    }

    fun checkInternetConnection(mContext: Context):Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = connectivityManager.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    }

    @JvmStatic
    fun hideKeyBoard(view: View, context: Context){
        try {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(view.getWindowToken(), 0)
        } catch (e: Exception) {
            Log.e("",e.toString())
        }
    }

    fun updateScreenView(viewModel: MainViewModel, context: Context, imageID: Int, textID: Int){
        viewModel.imageIconChange.set(context.getDrawable(imageID))
        viewModel.textChange.set(context.getString(textID))
        viewModel.layoutVisibility.set(true)
        viewModel.gridVisibility.set(false)
    }
}