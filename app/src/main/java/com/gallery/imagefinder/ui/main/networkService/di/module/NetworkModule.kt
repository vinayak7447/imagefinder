package com.gallery.imagefinder.ui.main.networkService.di.module



import com.gallery.imagefinder.ui.main.networkService.di.interceptor.AuthenticationInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


@Module
class NetworkModule() {

    @Provides
    internal fun getOkHttpClient(): OkHttpClient {
        var dispatcher = Dispatcher()
        dispatcher.maxRequests = 1
        var httpClient = OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .dispatcher(dispatcher)
            .addInterceptor(AuthenticationInterceptor())
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        return httpClient.build()
    }

}
