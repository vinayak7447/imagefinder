package com.gallery.imagefinder.ui.main.localDataBase.di.injector

import com.gallery.imagefinder.ui.main.localDataBase.di.component.IDBComponent


interface IDBInjector {
    fun roomDBInjector(idbComponent: IDBComponent)
}