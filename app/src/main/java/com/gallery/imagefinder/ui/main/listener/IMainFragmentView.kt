package com.gallery.imagefinder.ui.main.listener

import android.os.Bundle

interface IMainFragmentView {
    fun initView()
    fun onListItemClickListener(imageUrl: Bundle)

    interface IViewModelImpl{
        fun callImageFinderApi(searchContent:String)
    }
}