package com.gallery.imagefinder.ui.main.networkService.di.injector

import com.gallery.imagefinder.ui.main.networkService.di.component.ServiceComponent


interface IApiServiceInjector {
    fun apiServiceInjector(serviceComponent: ServiceComponent)
}
