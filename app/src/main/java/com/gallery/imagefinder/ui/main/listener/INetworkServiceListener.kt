package com.gallery.imagefinder.ui.main.listener

interface INetworkServiceListener {
    fun apiNetworkServiceReq(apiEndPoint:String, request:Any, onGetResponse: IOnGetParserResponseListener)
}