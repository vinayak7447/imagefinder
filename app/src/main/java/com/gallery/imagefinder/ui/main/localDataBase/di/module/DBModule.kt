package com.gallery.imagefinder.ui.main.localDataBase.di.module

import android.app.Application
import androidx.room.Room
import com.gallery.imagefinder.ui.main.localDataBase.listener.IRoomDao
import com.gallery.imagefinder.ui.main.localDataBase.dataBase.RoomDB
import com.gallery.imagefinder.ui.main.localDataBase.dataBase.UserCommentDataImpl
import com.gallery.imagefinder.ui.main.localDataBase.listener.IUserComment
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DBModule {

    @Singleton
    @Provides
    internal fun roomDatabase(mApplication: Application,dbName: String): RoomDB {
        return Room.databaseBuilder(
            mApplication.applicationContext,
            RoomDB::class.java, dbName
        ).allowMainThreadQueries().build()
    }

    @Singleton
    @Provides
    internal fun roomDao(roomDB: RoomDB): IRoomDao {
        return roomDB.roomDao
    }

    @Singleton
    @Provides
    internal fun userCommentDetails(roomDao: IRoomDao): IUserComment {
        return UserCommentDataImpl(roomDao)
    }

}