package com.gallery.imagefinder.ui.main.localDataBase.tableSchema

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
class ImageCommentData {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    var imageId: String? = null

    var commet: String? = null
}