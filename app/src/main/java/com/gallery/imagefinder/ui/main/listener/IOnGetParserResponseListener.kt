package com.gallery.imagefinder.ui.main.listener

interface IOnGetParserResponseListener {
    fun onGetSuccessResponse(response: String)
    fun onGetFailureResponse(response: String)
}