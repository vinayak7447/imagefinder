package com.gallery.imagefinder.ui.main.localDataBase.dataBase

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gallery.imagefinder.ui.main.localDataBase.dataBase.RoomDB.Companion.VERSION
import com.gallery.imagefinder.ui.main.localDataBase.listener.IRoomDao

import com.gallery.imagefinder.ui.main.localDataBase.tableSchema.ImageCommentData

@Database(entities = [ImageCommentData::class], version = VERSION)
abstract class RoomDB : RoomDatabase() {

    abstract val roomDao: IRoomDao

    companion object {

        internal const val VERSION = 1
    }

}
