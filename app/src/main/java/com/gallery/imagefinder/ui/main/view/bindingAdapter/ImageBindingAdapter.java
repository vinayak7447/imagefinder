package com.gallery.imagefinder.ui.main.view.bindingAdapter;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.gallery.imagefinder.R;
import com.gallery.imagefinder.ui.main.localDataBase.tableSchema.ImageCommentData;
import com.gallery.imagefinder.ui.main.view.gridLayout.AutoFitRecyclerView;

import java.util.List;

import kotlin.jvm.JvmStatic;

public class ImageBindingAdapter {

    private ImageBindingAdapter() {

    }

    @BindingAdapter({"bind:imageUrl"})
    @JvmStatic
    public static void setImageToGlide(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .centerCrop()
                .placeholder(R.drawable.ic_image_icon)
                .error(R.drawable.ic_image_icon)
                .fallback(R.drawable.ic_image_icon)
                .into(view);
    }

    @BindingAdapter({"bind:layoutVisibility"})
    @JvmStatic
    public static void layoutVisibility(LinearLayout view, Boolean isVisible) {
      if(isVisible)
          view.setVisibility(View.VISIBLE);
      else
          view.setVisibility(View.GONE);
    }

    @BindingAdapter({"bind:imageIconChange"})
    @JvmStatic
    public static void imageIconChange(ImageView view, Drawable resourceID) {
        view.setImageDrawable(resourceID);
    }

    @BindingAdapter({"bind:textChange"})
    @JvmStatic
    public static void textChange(TextView view, String msg) {
        view.setText(msg);
    }



    @SuppressLint("ResourceAsColor")
    @BindingAdapter({"bind:textSet"})
    @JvmStatic
    public static void textSet(LinearLayout view, List<ImageCommentData> imageCommentData) {
        view.removeAllViews();
        if(imageCommentData.size()>0) {
            final TextView rowTextView = new TextView(view.getContext());
            rowTextView.setTextSize(20);
            rowTextView.setPadding(20, 30, 20, 30);
            rowTextView.setTextColor(R.color.colorAccent);
            rowTextView.setTypeface(rowTextView.getTypeface(), Typeface.BOLD);
            rowTextView.setText(R.string.txt_comment);
            view.addView(rowTextView);
            for (ImageCommentData i : imageCommentData) {
                final CardView cardView = new CardView(view.getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(20, 15, 20, 15);
                cardView.setLayoutParams(layoutParams);
                cardView.setElevation(5);
                final TextView rowDynamicText = new TextView(view.getContext());
                rowDynamicText.setTextSize(17);
                rowDynamicText.setPadding(20, 30, 20, 30);
                rowDynamicText.setText(i.getCommet());
                cardView.addView(rowDynamicText);
                view.addView(cardView);
            }
        }
    }

    @BindingAdapter({"bind:gridVisibility"})
    @JvmStatic
    public static void gridVisibility(AutoFitRecyclerView view, boolean isVisible) {
        if(isVisible)
            view.setVisibility(View.VISIBLE);
        else
            view.setVisibility(View.GONE);
    }

}

