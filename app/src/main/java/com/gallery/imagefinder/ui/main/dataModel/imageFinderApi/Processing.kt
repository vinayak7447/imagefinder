package com.gallery.imagefinder.ui.main.dataModel.imageFinderApi

data class Processing(
    val status: String
)