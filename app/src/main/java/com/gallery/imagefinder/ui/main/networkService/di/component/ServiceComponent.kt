package com.gallery.imagefinder.ui.main.networkService.di.component

import com.gallery.imagefinder.ui.main.networkService.di.module.ServiceModule
import com.gallery.imagefinder.ui.main.listener.IApiService
import com.gallery.imagefinder.ui.main.networkService.serviceDao.NetworkServiceDao
import dagger.BindsInstance
import dagger.Component


@Component(modules = [ServiceModule::class])
interface ServiceComponent {

    @Component.Factory
    interface Factory{
        fun create(@BindsInstance serviceBaseURL: String): ServiceComponent
    }

    fun apiInterface(): IApiService

    fun injectNetworkService(networkServiceDao: NetworkServiceDao)
}