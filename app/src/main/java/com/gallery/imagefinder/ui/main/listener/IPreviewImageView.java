package com.gallery.imagefinder.ui.main.listener;

import android.view.View;

public interface IPreviewImageView {
    void onSubmitButtonClick(View view);
    void addCommentAtDB(String imageId, String comment);
    void getCommentFromDB(String imageId);

    interface IPreviewImageImpl{
        void onSubmitComment();
    }
}
