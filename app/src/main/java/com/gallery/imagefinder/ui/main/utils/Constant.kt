package com.gallery.imagefinder.ui.main.utils

object Constant {
    const val SUCCESS_CODE="200"
    const val AUTH_HEADER="Authorization"
    const val AUTH_HEADER_VALUE="Client-ID 137cda6b5008a7c"
    const val BASE_URL="https://api.imgur.com/"
    const val DATA_BASE_NAME="DBImageFinder"
    const val IMAGE_LIST_API_END_POINT="3/gallery/search/1?"
    const val QUERY_PARAMETER="q"
    const val BUNDLE_IMAGE_TAG="SelectedImageUrl"
    const val BUNDLE_ID_TAG="SelectedImageId"
    const val BUNDLE_IMAGE_TITLE_TAG="SelectedImageTitle"
    const val BUNDLE_TAG="SelectedImageValue"
}