package com.gallery.imagefinder.ui.main.localDataBase.listener

import com.gallery.imagefinder.ui.main.localDataBase.tableSchema.ImageCommentData


interface IUserComment {

    abstract fun setUserCommentDetails(imageComment: ImageCommentData): Long

    abstract fun getUserCommentDetails(): List<ImageCommentData>

    abstract fun getUserCommentDetailsById(imageId:String): List<ImageCommentData>
}