package com.gallery.imagefinder.ui.main.localDataBase.listener

import androidx.room.*
import com.gallery.imagefinder.ui.main.localDataBase.tableSchema.ImageCommentData


@Dao
interface IRoomDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setUserCommentDetails(imageCommentData: ImageCommentData): Long

    @Query("SELECT * FROM ImageCommentData")
    fun getUserCommentDetails(): List<ImageCommentData>

    @Query("SELECT * FROM ImageCommentData WHERE imageId IN (:imageId) ORDER BY id DESC")
    fun getUserCommentDetailsByID(imageId:String): List<ImageCommentData>

}