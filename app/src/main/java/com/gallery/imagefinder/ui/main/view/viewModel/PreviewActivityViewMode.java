package com.gallery.imagefinder.ui.main.view.viewModel;

import android.view.View;

import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;

import com.gallery.imagefinder.ui.main.listener.IPreviewImageView;
import com.gallery.imagefinder.ui.main.localDataBase.daoManager.RoomDBOperation;
import com.gallery.imagefinder.ui.main.localDataBase.tableSchema.ImageCommentData;

import java.util.List;

public class PreviewActivityViewMode extends ViewModel implements IPreviewImageView {

    public ObservableField<String> resultImageUrl = new ObservableField<>();
    public ObservableField<List<ImageCommentData>> comments = new ObservableField<>();
    public IPreviewImageView.IPreviewImageImpl iPreviewImage;

    public void imageUrlUpdated(String url) {
        resultImageUrl.set(url);
    }

    @Override
    public void onSubmitButtonClick(View view) {
        iPreviewImage.onSubmitComment();
    }

    @Override
    public void addCommentAtDB(String imageId, String comment) {
        ImageCommentData imageCommentData= new ImageCommentData();
        imageCommentData.setCommet(comment);
        imageCommentData.setImageId(imageId);
        RoomDBOperation.setCommentAtDB(imageCommentData);
        getCommentFromDB(imageId);
    }

    @Override
    public void getCommentFromDB(String imageId) {
        List<ImageCommentData> imageCommentData= RoomDBOperation.geCommentByImageIdFromDB(imageId);
        comments.set(imageCommentData);
    }
}
