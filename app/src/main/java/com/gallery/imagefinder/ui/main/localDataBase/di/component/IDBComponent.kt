package com.gallery.imagefinder.ui.main.localDataBase.di.component

import android.app.Application
import com.gallery.imagefinder.ui.main.localDataBase.daoManager.RoomDBOperation
import com.gallery.imagefinder.ui.main.localDataBase.di.module.DBModule
import com.gallery.imagefinder.ui.main.localDataBase.listener.IUserComment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DBModule::class])
interface IDBComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance mApplication: Application,@BindsInstance dbName: String): IDBComponent
    }

    fun injectDetails(dbValues: RoomDBOperation)

    fun getUserComments(): IUserComment

}